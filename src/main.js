global.jQuery = require('jquery');
global.$ = global.jQuery;
require('bootstrap');
require('./static/jstree.min.js');
//////////////////////////////////////////////////////////////////////////////

// Start UI manager
require('./ui.js');

// Start backends
require('./back.savefile.js');
require('./back.storage.js');

// Start Services
require('./service.notify.js');
require('./service.pgp.js');
require('./service.compose.js');
require('./service.read.js');
