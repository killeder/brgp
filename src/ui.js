/*
 * Mount and switch sections and tabs
 * ----------------------------------
 *
 * Sections are left-column listed tasks, Tabs are switched inside sections.
 *
 * UI system accepts per `postal.js` commands to switch between sections
 * and tabs.
 */


var riot = require('riot'),
    postal = require('./lib/postal');
require('../build/views.js');
var $ui$ = postal.channel('ui');


var ui = riot.mount('body')[0];
$ui$.subscribe('cmd:switch-section', function(e){
    ui.update({ section: e });
});

$(function(){ $ui$.publish('evt:init'); });
