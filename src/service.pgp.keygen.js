var riot = require('riot'),
    openpgp = require('./lib/openpgp'),
    postal = require('./lib/postal');
var $pgp$ = postal.channel('pgp');

function pgpKeygen(){
    var self = this;
    riot.observable(this);

    var ui = riot.mount('ui-pgp-keygen')[0];

    function onSubmission(el){
        var options = {
            keyType: 1, // RSA encrypt + sign
            numBits: parseInt(el.bits),
            userIds: [el.userid],
            passphrase: el.passphrase,
        };
        openpgp.generateKey(options).then(function(key){
            var privKey = key.privateKeyArmored,
                pubKey = key.publicKeyArmored;
            console.debug("Keypair generated", key);
            $pgp$.publish('cmd:keyring-add', key.key); 
            ui.update({ privateKey: privKey, publicKey: pubKey });
            ui.update({ generating: false });
        }, function(){
            ui.update({ generating: false });
        });
    }


    ui.on('submission', onSubmission);

    return this;
}

module.exports = pgpKeygen;
