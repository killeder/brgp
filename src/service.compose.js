var riot = require('riot'),
    postal = require('./lib/postal');

var $compose$ = postal.channel('compose'),
    $ui$ = postal.channel('ui'),
    $notify$ = postal.channel('notify');
var openpgp = require('./lib/openpgp'),
    openpgpUtil = require('./openpgp.util.js');

function Compose(){
    // `Compose` service controls the backend behaviour for the UI `Compose a
    // Message` section. User may select designated receivers and signers from
    // the `pgp.keyring` section, whose backend will send user's decision per
    // `postal` to this on channel `compose` using an event `cmd:set-keyids`.
    //
    // Remember that until user finally decided to `generate` the ciphertext,
    // we DO NOT CACHE any choosen keyIDs in this backend. Rather, we store
    // them ONLY in UI frontend, on the consideration that otherwise displayed
    // keyIDs may out-of-sync, which will cheat the user.
    //
    // Because we have used `postal.js` as message bus, we have to query for
    // the pgp keys here again, not just getting the key instances passed.

    var self = this;
    var ui = riot.mount('ui-compose')[0];

    $compose$.subscribe('cmd:set-keyids', function(e){
        ui.update({ 
            encrypt: openpgpUtil.queryKeyInfoFromIDs(e.encrypt),
            sign: openpgpUtil.queryKeyInfoFromIDs(e.sign),
        });
        $ui$.publish('cmd:switch-section', 'compose');
    });

    ui.on('generate', function(e){
        /* generate ciphertext */
        var error = {};
        
        // prepare public keys for encrypt
        var publicKeys = [];
        var queried = openpgpUtil.queryKeysFromIDs(e.encrypt);
        for(var i in queried) publicKeys.push(queried[i]);

        // prepare private keys for signing
        var privateKeys = [];
        var queried = openpgpUtil.queryKeysFromIDs(e.sign);
        for(var keyid in queried){
            var key = queried[keyid];
            if(!key.isDecrypted){ // if not decrypted yet: decrypt this key
                if(!key.decrypt(e.password[keyid])){ // if key decryption fails
                    console.error("Wrong passphrase for key: " + keyid);
                    $notify$.publish('err:decrypt-private-key', key);
                    error.password = keyid;
                    ui.update({ error: error });
                    return;
                }
            }
            privateKeys.push(key);
        };

        // do processing the data

        var message = openpgp.message.fromText(e.content);

        if(privateKeys.length > 0){ // sign
            try{
                message = message.sign(privateKeys);
                console.debug('signed');
            } catch(e){
                console.error('error signing', e);
                return ui.update({ error: e });
            }
        }

        if(publicKeys.length > 0){ // encrypt
            message.encrypt(publicKeys).then(function(msg){
                console.log('encrypted', msg);
                ui.update({ result: msg.armor() });
            }, function(err){
                console.error('error encrypting', err);
                ui.update({ error: err });
            });
            return;
        }

        ui.update({ result: message.armor() });
    });

    return this;
}

var compose = new Compose();
