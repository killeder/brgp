var riot = require('riot');
riot.mount('ui-pgp');

var pgpKeygen = require('./service.pgp.keygen.js'),
    pgpKeyring = require('./service.pgp.keyring.js'),
    pgpImport = require('./service.pgp.import.js');

var keygenInstance = new pgpKeygen(),
    keyringInstance = new pgpKeyring(),
    keyimportInstance = new pgpImport();
