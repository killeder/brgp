<ui-pgp-keygen>

    <form class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Username</label>
            <div class="col-sm-9">
                <input class="form-control" name="username" type="text" />
            </div>
            <div if={ err.userid_invalid }>
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <p class="help-block">Invalid User ID.</p>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <input class="form-control" name="email" type="text" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Key Security Bits</label>
            <div class="col-sm-9">
                <select class="form-control" name="bits">
                    <option value="2048">2048 - medium security</option>
                    <option value="4096">4096 - high security</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Enter Passphrase</label>
            <div class="col-sm-9">
                <input class="form-control" name="passphrase" type="password" />
            </div>
            <div if={ err.passphrase_too_short }>
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <p class="help-block"> Passphrase too short.  </p>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Repeat Passphrase</label>
            <div class="col-sm-9">
                <input class="form-control" name="passphrase2" type="password" />
            </div>
            <div if={ err.passphrase_not_match }>
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <p class="help-block"> Passphrases not match.  </p>
                </div>
            </div>
        </div>

        <div>
            <button class="btn btn-primary" type="button" onclick={ submit } disabled={ generating }>
                Generate
            </button>
        </div>

    </form>

    <div if={ privateKey && publicKey }>
        <div><textarea class="privatekey">{ privateKey }</textarea></div>
        <div><textarea class="publickey">{ publicKey }</textarea></div>
    </div>

<script>

this.err = {
    userid_invalid: false,
    passphrase_too_short: false,
    passphrase_not_match: false,
}
this.generating = false;

submit(el){
    this.err.userid_invalid = (this.username.value.length < 3);
    this.err.passphrase_too_short = (this.passphrase.value.length < 5);
    this.err.passphrase_not_match = (
        this.passphrase.value != this.passphrase2.value
    );
    for(var i in this.err){ if(this.err[i]) return; }
    
    var req = {
        userid: {
            name: this.username.value,
            email: this.email.value,
        },
        bits: this.bits.value,
        passphrase: this.passphrase.value,
    };

    this.generating = true;
    this.privateKey = null;
    this.publicKey = null;

    this.trigger("submission", req);
}

</script>
</ui-pgp-keygen>
