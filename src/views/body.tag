<body>
<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>BrGP <small>Browser-based Good Privacy</small></h1>
    </div>
</div>
<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <ul class="nav nav-pills nav-stacked">
            <li role="presentation" class="active">
                <a href="#pgp" data-toggle="pill">Key Management</a>
            </li>
            <li role="presentation">
                <a href="#compose" data-toggle="pill">Compose a Message</a>
            </li>
            <li role="presentation">
                <a href="#read" data-toggle="pill">Read a Message</a>
            </li>
            <li role="presentation">
                <a href="#help" data-toggle="pill">Help</a>
            </li>
        </ul>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="pgp">
                <ui-pgp></ui-pgp>
            </div>
            <div role="tabpanel" class="tab-pane" id="compose">
                <ui-compose></ui-compose>
            </div>
            <div role="tabpanel" class="tab-pane" id="read">
                <ui-read></ui-read>
            </div>
            <div role="tabpanel" class="tab-pane" id="help">
            
            </div>
        </div>

    </div>
</div></div>





<script>

this.section = "pgp";
this.on("update", function(e){
    if(e && e.section){
        $(this.root).find('a[href="#' + this.section + '"]').tab('show');
    }
});

</script>
</body>
