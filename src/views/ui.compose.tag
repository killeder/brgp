<ui-compose>

<div class="panel-group" id="compose" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="compose-encrypt-head">
            <h4 class="panel-title">
                <a 
                    role="button"
                    data-toggle="collapse"
                    data-parent="#compose"
                    href="#compose-encrypt"
                    aria-expanded="true"
                    aria-controls="compose-encrypt"
                >
                    1. Encrypt to whom?
                </a>
            </h4>
        </div>
        <div id="compose-encrypt" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="compose-encrypt-head">
            <div class="panel-body" if={ Object.keys(_encrypt).length < 1 }>
                Currently no recipients defined. The message would not be encrypted.
                Go to "Key Management" and add some public keys here.
            </div>
            <ul class="list-group" if={ Object.keys(_encrypt).length > 0 }>
                <li each={ id, key in _encrypt } data-id="enc-{ id }" class="list-group-item">
                    { key.primaryUserid }
                    <button type="button" data-remove="enc-{ id }" class="btn btn-link">Remove</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="compose-sign-head">
            <h4 class="panel-title">
                <a
                    class="collapsed"
                    role="button"
                    data-toggle="collapse"
                    data-parent="#compose"
                    href="#compose-sign"
                    aria-expanded="false"
                    aria-controls="compose-sign"
                >
                    2. Sign with?
                </a>
            </h4>
        </div>
        <div id="compose-sign" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="compose-sign-head">
            <div class="panel-body" if={ Object.keys(_sign).length < 1 }>
                Currently no signers defined. The message would not be signed.
                Go to "Key Management" and add some private keys here.
            </div>
            <ul class="list-group" if={ Object.keys(_sign).length > 0 }>
                <li each={ id, key in _sign } data-id="sig-{ id }" class="list-group-item">
                    { key.primaryUserid }
                    <input if={ !key.isDecrypted } data-id="sig-{ id }-pwd" type="password" class="form-control" />
                    <!--<div if={ id == _error.password } class="alert alert-danger">Invalid password.</div>-->
                    <button type="button" data-remove="sig-{ id }" class="btn btn-link">Remove</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="compose-content-head">
            <h4 class="panel-title">
                <a
                    class="collapsed"
                    role="button"
                    data-toggle="collapse"
                    data-parent="#accordion"
                    href="#compose-content"
                    aria-expanded="false" 
                    aria-controls="compose-content"
                >
                    3. Write your text...
                </a>
            </h4>
        </div>
        <div id="compose-content" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="compose-content-head">
            <div class="panel-body">
                <textarea name="content" class="form-control"></textarea>
                <button
                    type="button"
                    class="btn btn-primary"
                    onclick={ submit }
                    disabled={ generating || (Object.keys(_encrypt).length < 1 && Object.keys(_sign).length < 1) }
                >
                    Generate my text!
                </button>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="compose-result-head">
            <h4 class="panel-title">
                <a
                    class="collapsed"
                    role="button"
                    data-toggle="collapse"
                    data-parent="#accordion"
                    href="#compose-result"
                    aria-expanded="false" 
                    aria-controls="compose-result"
                >
                    4. Get the result!
                </a>
            </h4>
        </div>
        <div id="compose-result" class="panel-collapse collapse" role="tabpanel" aria-labelledby="compose-result-head">
            <div class="panel-body" if={ '' != result }>
                <textarea
                    name="result-display" 
                    class="form-control"
                    readonly="readonly"
                ></textarea>
            </div>
            <div class="panel-body" if={ '' == result }>
                Currently no ciphertext generated.
            </div>
        </div>
    </div>
</div>



<script>

var self = this;
var root = this.root;

this.generating = false;
this.result = '';
this._encrypt = {};
this._sign = {};
this._error = {};

function switchToResult(){
    $(self.root).find('#compose-content').collapse('hide');
    $(self.root).find('#compose-result').collapse('show');
}

function switchToContent(){
    $(self.root).find('#compose-content').collapse('show');
    $(self.root).find('#compose-result').collapse('hide');
    self.result = '';
    self.update();
}

this.on('update', function(e){
    if(e && (e.encrypt || e.sign)){
        switchToContent();
        if(e.encrypt){
            for(var i in e.encrypt) this._encrypt[i] = e.encrypt[i];
        }
        if(e.sign){
            for(var i in e.sign) this._sign[i] = e.sign[i];
        }
    }
    if(e && (e.result || e.error)){
        this.generating = false;
        if(e.error){
            this._error.password = e.error.password || '';
        }
        if(e.result){
            this._error = {};
            switchToResult();
        }
    }
});

this.on('updated', function(e){
    $(root).find('button[data-remove]').off('click').click(function(e){
        var itemid = $(this).attr('data-remove'),
            group = itemid.slice(0,3),
            keyid = itemid.slice(4);
        if('enc' == group) delete self._encrypt[keyid];
        if('sig' == group) delete self._sign[keyid];
        switchToContent();
    });
    $(this.root).find('[name="result-display"]').val(self.result);
});

submit(el){
    var password = {};
    var submission = {
        content: this.content.value,
        encrypt: Object.keys(this._encrypt),
        sign: Object.keys(this._sign),
    };
    for(var keyid in this._sign){
        password[keyid] = $('input[data-id="sig-' + keyid + '-pwd"]').val();
    }
    submission.password = password;

    this.generating = true;
    this.trigger('generate', submission);
}

</script>
</ui-compose>
