<ui-pgp-keyring-manage>

<div class="modal fade" tabindex="-1" role="dialog" name="dialog-manage">
    <div class="modal-dialog"> <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Manage Key</h4>
        </div>
        <div class="modal-body">

            <div class="well well-sm">
                <div name="keyinfo" class="pgpkeyinfo"></div>
            </div>
            
            <div class="well well-sm" name="description"></div>

        </div>
        <div class="modal-footer">
            <button name="dialog-close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div></div>
</div>


<div name="descriptions" style="display: none">
    <div data-id="pubprv">
        <span if={ info.isPrivate }>
            This is a private key. 
            You should keep it's data secret and secure by exporting.
            <strong>
                If you lost a private key, you will no longer be able to
                decrypt messages people sent to you, or to make any signature.
            </strong>
        </span>
        <span if={ info.isPublic }>
            This key is a public key. You may export and distribute it freely.
        </span>
    </div>

    <div data-id="userid">
        UserID claims the user of a key.
        <p />
        <strong>
            Unless otherwise confirmed, you should NOT trust a key belonging to
            anyone.
        </strong>
        Unfold a UserID and read signatures from holders of other keys.
    </div>

    <div data-id="primarykey-status">
        <span if={ info.primaryKeyStatus.valid } class="green">
            This primary key is verified as valid.
        </span>
        <span if={ !info.primaryKeyStatus.valid } class="red">
            The key is verified as invalid. Reason(s):
            <span if={ info.primaryKeyStatus.expired }>
                The key is expired.
            </span>
            <span if={ info.primaryKeyStatus.revoked }>
                The key is revoked.
            </span>
        </span>
        <p />
        Status are determined on several verifications. A valid status means
        (1) the key is self-signed (2) the key is not revoked (3) the key is
        not expired. Any other status means a key should not be used,
        especially for encrypting and signing.
    </div>

    <div each={ userId, i in info.userIds }>
        <div data-id="userid-{ i }-selfsigvalid">
            <span if={ userId.validSelfSigned } class="green">
                We have found at least one valid self signature.
            </span>
            <span if={ !userId.validSelfSigned }>
                <span class="red">
                    No self signature on this key is found to be valid.
                </span>
                You should not assume this identity is being used by the owner
                of this key.
            </span>
            <p />
            Self signatures on a UserID is a statement of key holder on the
            ownership of this identity. However it doesn't assure any
            information on the realness.
        </div>

        <div data-id="userid-{ i }-othersig">
            Signatures from other key owners. Each signature on this given
            UserID is an acknowledgement of the ownership of this UserID by the
            holder of primary key.
        </div>

        <div each={ cert, j in info.userIds[i].otherCertifications }>
            <div data-id="userid-{ i }-othersig-{ j }">
                <span if={ cert.verified === null }>
                    We do not have the public key of issuer to verify this
                    signature.
                </span>
                <span if={ cert.verified === true } class="green">
                    Signature verified to be valid.
                </span>
                <span if={ cert.verified === false } class="red">
                    Signature found to be invalid.
                </span>
            </div>
        </div>
    </div>

    <div data-id="action-changepassphrase" if={ info.isPrivate }>
        To change the passphrase of your key, input the old and new passphrase.
        If you have previously exported your private key, remember to update
        it with another export.

        <div class="alert alert-danger" if={ error.newPassphrasesNotMatch }>
            New passphrases do not match.
        </div>
        <div class="alert alert-danger" if={ error.newPassphraseTooShort }>
            New passphrase too short.
        </div>

        <form>
            <div class="form-group">
                <label>Old passphrase</label>
                <input name="oldpassphrase" class="form-control" type="password" />
            </div>
            <div class="form-group">
                <label>New passphrase</label>
                <input name="newpassphrase" class="form-control" type="password" />
            </div>
            <div class="form-group">
                <label>Type new passphrase again</label>
                <input name="newpassphrase2" class="form-control" type="password" />
            </div>
            <button 
                type="button"
                name="action-changepassphrase"
                class="btn btn-primary"
                onclick={ confirmChangePassphrase }
            >
                Change Passphrase
            </button>
        </form>
    </div>

    <div data-id="action-delete">
        <span if={ info.isPrivate }>
            <span class="red">
                <p><strong>
                    Dangerous action! You are deleting a PRIVATE key!
                </strong></p>
                If you have not yet backed up this private key, after deletion
                you'll NEVER recover it!
            </span>
            You'll not be able to decrypt messages encrypted with this key
            anymore. You'll not be able to make any signatures related to
            this key!
        </span>
        <span if={ !info.isPrivate }>
            <strong>Caution</strong>
            After deletion you will not be able to send encrypted messages
            to this key's owner. Messages signed with this key will not be
            verified.
            <p />
            If you are trying to delete a public key, whose private key also
            exists in our keyring, you will have to delete the private part
            first.
        </span>
        <p />
        <span if={ info.isPrivate }>
            This is the final warning! To continue deletion, repeat
            <strong>DELETE 0x{ info.primaryKeyFingerprint.slice(-8) }</strong>
            in following box, and click the button.
            <input class="form-control" name="delete-key-confirm" data-confirm="DELETE 0x{ info.primaryKeyFingerprint.slice(-8) }" />
        </span>
        <span if={ !info.isPrivate }>
            <input
                type="hidden"
                name="delete-key-confirm"
                data-confirm="DELETE 0x{ info.primaryKeyFingerprint.slice(-8) }"
                value="DELETE 0x{ info.primaryKeyFingerprint.slice(-8) }"
            />
        </span>
        <button class="btn btn-danger" name="action-delete-confirm" onclick={ confirmDeleteKey }>Confirm Deletion</button>
    </div>

</div>


<script>

var self = this;
this.error = {};

function jstreeLocate(){
    return $(self.root).find('[name="keyinfo"]');
}

function label(l, v){
    return "<strong>" + l + "</strong> " + $('<div>').text(v).html();
}

function dblabel(l, vg, vr){
    var p = (Boolean(l) ? $('<strong>').text(l).prop('outerHTML') + ' ' : '');
    if(vg){
        return (p
            + '<span class="green">' + $('<div>').text(vg).html() + '</span>'
        );
    }
    return p + '<span class="red">' + $('<div>').text(vr).html() + '</span>';
}

this.on('update', function(e){
    if(e && e.manageKey){
        $(this.root).find('[name="dialog-manage"]').modal('show');
        this.info = e.manageKey.key;
        init(e.manageKey.key, e.manageKey.options);
    }
});

this.one('mount', function(){
    jstreeLocate().click(function(){
        var sel = jstreeLocate().jstree('get_selected');
        if(sel.length < 1) return;
        $(self.root).find('[name="description"]').find('[data-id]')
            .detach()
            .appendTo('[name="descriptions"]')
        ;
        $(self.root).find('[data-id="' + sel[0] + '"]')
            .detach()
            .appendTo($('[name="description"]').empty())
        ;
    });
    $(this.root).find('.dialog-modal').on("hidden.bs.modal", function(){
        self.unmount();
    });
});

confirmDeleteKey(){
    // find confirmation input only in displayed description area.
    var userConfirm = 
        $('[name="description"]').find('[name="delete-key-confirm"]');
    if(userConfirm.length < 1) return;
    if(userConfirm.val() === userConfirm.attr('data-confirm')){
        this.trigger('delete');
        $(self.root).find('button[name="dialog-close"]').click();
    }
}

confirmChangePassphrase(){
    var s = '[name="description"]';
    var oldp = $(s).find('[name="oldpassphrase"]').val(),
        newp = $(s).find('[name="newpassphrase"]').val(),
        newp2 = $(s).find('[name="newpassphrase2"]').val();
    this.update({ error: {}});
    if(newp != newp2){
        return this.update({ error: { newPassphrasesNotMatch: true }});
    }
    if(newp.length < 5){
        return this.update({ error: { newPassphraseTooShort: true }});
    }
    this.trigger('changePassphrase', {
        keyid: this.info.keyId,
        oldPassphrase: oldp,
        newPassphrase: newp,
    });
    $(self.root).find('button[name="dialog-close"]').click();
}

// ---------- Parse key and descriptions

function init(info){
    $(self.root).find('[name="description"]')
        .text('Choose one item to see description here.')
    ;
    jstreeLocate().jstree('destroy');
    jstreeLocate().empty().jstree({
        plugins: ['wholerow', 'conditionalselect'],
        conditionalselect: function(node, event){
            return ($('[data-id="' + node.id + '"]').length > 0);
        },
        core: { data: parseInfo(info), },
    });
}

function parseInfo(info){
    var l = [], tempid, tempid2, tempid3, cert;

    // ---- Status as Public or Private Key
    
    l.push({
        id: 'pubprv',
        parent: '#',
        text: dblabel(
            "Key Type",
            (!info.isPrivate ? "Public Key": false),
            "Private Key"
        )
    });

    // ---- User Ids

    l.push({
        id: 'userid',
        parent: '#',
        text: 'UserID',
        state: { opened: true },
    });
    for(var i in info.userIds){
        tempid = 'userid-' + i;
        l.push({
            id: tempid,
            parent: 'userid',
            text: label('', info.userIds[i].userid),
        });
        l.push({
            id: tempid + '-selfsigvalid',
            parent: tempid,
            text: dblabel(
                'Self signature', 
                (info.userIds[i].validSelfSigned ? "Found": false),
                "No"
            ),
        });

        tempid2 = tempid + '-othersig';
        l.push({
            id: tempid2,
            parent: tempid,
            text: label(
                'Signatures from others',
                '(' + info.userIds[i].otherCertifications.length + ')'
            ),
        });
        for(var j in info.userIds[i].otherCertifications){
            cert = info.userIds[i].otherCertifications[j];
            tempid3 = tempid2 + '-' + j,
            l.push({
                id: tempid3,
                parent: tempid2,
                text: (
                    cert.issuer ? (
                        dblabel(
                            ('[' + cert.issuerKeyID + '] ' + cert.issuer),
                            (cert.verified ? 'Valid' : false),
                            'Invalid'
                        )
                    ) : '(unknown issuer)'
                ),
            });
            l.push({
                id: tempid3 + '-created',
                parent: tempid3,
                text: label('Creation', cert.created.toISOString()),
            });
        }
    }

    // ---- Primary Key Related Details

    l.push({
        id: 'primarykey',
        parent: '#',
        text: 'Primary Key',
        state: { opened: true },
    });
    l.push({
        id: 'primarykey-fp', 
        parent: 'primarykey', 
        text: label("Fingerprint", info.primaryKeyFingerprint),
    });
    l.push({
        id: 'primarykey-status',
        parent: 'primarykey',
        text: dblabel(
            "Status",
            (info.primaryKeyStatus.valid ? "Valid" : false),
            (info.primaryKeyStatus.revoked ? 
                "Revoked" :
                (info.primaryKeyStatus.expired ? "Expired" : "Invalid")
            )
        ),
    });
    l.push({
        id: 'primarykey-created',
        parent: 'primarykey',
        text: label('Creation', info.primaryKeyCreated.toISOString())
    });

    // ---- Sub Key Related Details

    l.push({
        id: 'subkey',
        parent: '#',
        text: 'Sub Key',
    });
    for(var i in info.subkeys){
        tempid = 'subkey-' + i, subkey = info.subkeys[i];
        l.push({
            id: tempid,
            parent: 'subkey',
            text: subkey.algorithm,
        });
        l.push({
            id: tempid + '-fp', 
            parent: tempid, 
            text: label("Fingerprint", subkey.fingerprint),
        });
        l.push({
            id: tempid + '-status',
            parent: tempid,
            text: dblabel(
                "Status",
                (subkey.status.valid ? "Valid" : false),
                (subkey.status.revoked ? 
                    "Revoked" :
                    (subkey.status.expired ? "Expired" : "Invalid")
                )
            ),
        });
        l.push({
            id: tempid + '-created',
            parent: tempid,
            text: label('Creation', subkey.created.toISOString())
        });
    };

    // ---- Actions

    l.push({
        id: 'action',
        parent: '#',
        text: "Actions",
    });
    l.push({
        id: 'action-changepassphrase',
        parent: 'action',
        text: "Change passphrase",
        state: {
            disabled: !info.isPrivate,
        }
    });
    l.push({
        id: 'action-delete',
        parent: 'action',
        text: dblabel("", false, "Delete this key"),
    });
    
    return l;
}




</script>
</ui-pgp-keyring-manage>
