<ui-pgp>

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#pgpkeyring" data-toggle="pill">All</a></li>
        <li role="presentation"><a href="#pgpkeygen" data-toggle="pill">Generate</a></li>
        <li role="presentation"><a href="#pgpimport" data-toggle="pill">Import</a></li>
    </ul>


    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="pgpkeyring">
            <ui-pgp-keyring></ui-pgp-keyring>
        </div>
        <div role="tabpanel" class="tab-pane" id="pgpkeygen">
            <ui-pgp-keygen></ui-pgp-keygen>
        </div>
        <div role="tabpanel" class="tab-pane" id="pgpimport">
            <ui-pgp-import></ui-pgp-import>
        </div>
    </div>


</ui-pgp>
