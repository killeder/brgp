/*
 * Permanent storage solution for `openpgp.memory.js`. Listens on channel
 * $storage$ for data operation events. Also emits an initialization event at
 * program start up.
 */

var $storage$ = require('./lib/postal').channel('storage');
var prefix = require('./openpgp.memory.js').prototype.prefix;

function storageAvailable(type){
    try {
        var storage = window[type],
        x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch(e) {
        return false;
    }
}

$(function init(){
    if(typeof window !== 'undefined'){
        var testLocalStorage = storageAvailable('localStorage');
        if(testLocalStorage){
            useLocalStorage();
            return;
        } else {
            usePostMessage();
            return;
        }
    }
});

//////////////////////////////////////////////////////////////////////////////

function useLocalStorage(){
    $storage$.subscribe('evt:dump', function(dump){
        var removeItems = [];
        for(var name in window.localStorage){
            if(name.slice(0, prefix.length) != prefix) continue;
            if(!dump[name]) removeItems.push(name);
        }
        for(var name in dump){
            window.localStorage.setItem(name, dump[name]);
        }
        removeItems.forEach(function(i){
            window.localStorage.removeItem(i);
        });
    });

    var init = {};
    for(var name in window.localStorage){
        if(name.slice(0, prefix.length) != prefix) continue;
        init[name] = window.localStorage.getItem(name);
    }
    $storage$.publish('cmd:init', init);
}

function usePostMessage(){
    $storage$.subscribe('evt:dump', function(dump){
        window.parent.postMessage({
            event: "dump",
            data: dump,
        }, window.location.origin);
    });

    window.addEventListener('message', function(e){
        console.debug("BrGP window on message", e);
        e = e.data;
        if(e.event == 'init'){
            $storage$.publish('cmd:init', e.data);
        }
    });
    window.parent.postMessage({ event: "sync", }, window.location.origin);
}
