/*
 * Service for managing a global keyring and UI asset(s) for displaying it.
 */
var riot = require('riot'),
    openpgp = require('./lib/openpgp')
    openpgpUtil = require('./openpgp.util.js'),
    postal = require('./lib/postal');
var $ui$ = postal.channel('ui'),
    $pgp$ = postal.channel('pgp'),
    $compose$ = postal.channel('compose'),
    $notify$ = postal.channel('notify'),
    $storage$ = postal.channel('storage');

//////////////////////////////////////////////////////////////////////////////

var keyring; 

function reloadKeyring(){
    keyring = openpgpUtil.getKeyring();
    $pgp$.publish('evt:keyring-updated');
}

// listen for upstream storage update event, important though awful!
$storage$.subscribe('evt:update', reloadKeyring);
$ui$.subscribe('evt:init', reloadKeyring); // TODO may subject to removal


function listKeys(includePrivate, includePublic){
    var list = [];
    function extractInfo(from){
        list.push(openpgpUtil.extractKeyInfo(from));
    }
    if(includePrivate){
        keyring.privateKeys.keys.forEach(extractInfo);
    }
    if(includePublic){
        keyring.publicKeys.keys.forEach(extractInfo);
    }
    console.debug("List keys:", list, keyring);
    return list;
}


$pgp$.subscribe('cmd:keyring-add', function(e){
    console.debug("Keyring-add:", e);
    // private key importing
    if(e.isPrivate()){
        keyring.privateKeys.importKey(e.armor());
    }
    // public key always cached
    keyring.publicKeys.importKey(e.toPublic().armor());

    keyring.store();
    reloadKeyring();
});

$pgp$.subscribe('cmd:keyring-remove', function(e){
    console.debug("Keyring-remove:", e);
    var keyid = e.keyid, isPrivate = e.isPrivate;
    keyring = openpgpUtil.getKeyring();
    if(!isPrivate){
        // prevent deleting a public key with private part existing
        var findPrivatePart = keyring.privateKeys.getForId(keyid, true);
        if(null !== findPrivatePart) return;

        // delete from public key
        keyring.publicKeys.removeForId(keyid);
    } else {
        keyring.privateKeys.removeForId(keyid);
    }
    keyring.store();
    reloadKeyring();
});

$pgp$.subscribe('cmd:keyring-changepassphrase', function(e){
    var keyid = e.keyid, oldPassphrase = e.oldPassphrase,
        newPassphrase = e.newPassphrase;
    
    keyring = openpgpUtil.getKeyring();
    var key = keyring.privateKeys.getForId(keyid, true);
    if(!key) return;
    
    if(!key.decrypt(oldPassphrase)){
        $notify$.publish('err:decrypt-private-key', key);
        return;
    }
    try{
        key.encrypt(newPassphrase);
        var armored = key.armor();
        keyring.privateKeys.importKey(armored);
    } catch(e){
        return;
    }

    keyring.store();
    reloadKeyring();
    $notify$.publish('evt:key-passphrase-changed', key);
    console.debug('Changed key passphrase.');
});


//////////////////////////////////////////////////////////////////////////////

function pgpKeyring(){
    var self = this;
    riot.observable(this);

    var ui = riot.mount('ui-pgp-keyring')[0],
        uiExport = riot.mount('ui-pgp-keyring-export')[0];

    function updateData(){
        var keylist = listKeys(true, true);
        ui.update({ data: keylist });
    }
    $pgp$.subscribe('evt:keyring-updated', updateData);

    function showKeyDetails(keyid, isPrivate){
        var uiManage = riot.mount(
            '#ui-pgp-keyring-manage',
            'ui-pgp-keyring-manage'
        )[0];

        var keyring = openpgpUtil.getKeyring(), key = false;
        if(isPrivate)
            key = keyring.privateKeys.getForId(keyid, true);
        else
            key = keyring.publicKeys.getForId(keyid, true);
        if(!key) return;

        var keyReport = openpgpUtil.examineKey(key, keyring.publicKeys);
        console.debug('key report', keyReport);
        uiManage.update({ manageKey: {
            key: keyReport,
            options: { } 
        }});
        
        uiManage.on('delete', function(){
            $pgp$.publish('cmd:keyring-remove', {
                keyid: keyid,
                isPrivate: isPrivate,
            });
        });

        uiManage.on('changePassphrase', function(e){
            $pgp$.publish('cmd:keyring-changepassphrase', {
                keyid: e.keyid,
                oldPassphrase: e.oldPassphrase,
                newPassphrase: e.newPassphrase,
            });
        });
    }

    ui.on('compose', function(e){
        // when user selects a range of receivers and signers for composing
        // a message.
        // the selected key IDs will be forwared to the `compose` section using
        // $compose$ channel.
        var encrypt = e.encrypt, sign = e.sign;
        $compose$.publish('cmd:set-keyids', { encrypt: encrypt, sign: sign });
    });

    ui.on('export', function(e){
        var pub = e.pub, prv = e.prv;
        var pubKeys = openpgpUtil.queryKeysFromIDs(pub),
            prvKeys = openpgpUtil.queryKeysFromIDs(prv);
        var ret = { publicKeyArmored: '', privateKeyArmored: ''};
        if(pub.length > 0){
            ret.publicKeyArmored = openpgpUtil.armorMultipleKeysOfSameType(
                pubKeys,
                false
            );
        }
        if(prv.length > 0){
            ret.privateKeyArmored = openpgpUtil.armorMultipleKeysOfSameType(
                prvKeys, 
                true
            );
        }
        uiExport.update(ret);
    });

    ui.on('manage', function(e){
        if(e.prv.length > 0)
            showKeyDetails(e.prv[0], true);
        else if(e.pub.length > 0)
            showKeyDetails(e.pub[0], false);
    });

    return this;
}

module.exports = pgpKeyring;
