all: build/index.html

build: build/index.min.html

clean:
	rm -rf build/*
	
build/index.min.html: build/index.pack.html
	html-minifier \
	--minify-css \
	--remove-comments \
	--collapse-inline-tag-whitespace \
	--collapse-whitespace \
	--conservative-collapse \
	build/index.pack.html > build/index.min.html
	
build/index.pack.html: build/index.html build/main.js pack.py
	python pack.py

build/index.html: src/index.html src/static/* build/main.js
	cp src/index.html build/index.html
	cp -r src/static build/

build/main.js: src/*.js src/lib/*.js build/views.js
	browserify src/main.js -o build/main.js

build/views.js: src/views/*.tag
	mkdir -p build
	riot -m src/views build/views.js
